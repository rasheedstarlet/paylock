from django.urls import path

from . import views

from . views import create_product_view, product_detail_view, cart, checkout

app_name = "products"

urlpatterns = [
	path('', views.products, name="products"),
	path('create-product', create_product_view, name="create_product"),
	path('product/<int:pk>/', product_detail_view.as_view(), name='post_detail'),
	path('cart', cart, name="cart"),
	path('checkout', checkout, name="checkout")
]