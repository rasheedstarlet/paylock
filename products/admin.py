from django.contrib import admin

from . models import Product

class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'price')
    prepopulated_fields = {'slug': ('name',)}
    raw_id_fields = ('user',)
    search_fields = ('user', 'name', 'description')
    date_hierarchy = ('created')
    list_filter = ( 'user', 'name', 'created', 'price')


admin.site.register(Product, ProductAdmin)