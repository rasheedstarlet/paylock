# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django import views

from django.views.generic import DetailView

from . models import Product

from . forms import NewProductForm

from django.contrib.auth.decorators import login_required

# Create your views here.
def products(request):
	products = Product.objects.all()
	return render(request, "products/products.html",  { 'products':products });

#@login_required(login_url='account:login')
def create_product_view(request):
	if request.method == "POST":
		form = NewProductForm(request.POST, request.FILES)
		if form.is_valid():
			instance = form.save(commit=False)
			instance.user = request.user
			instance.save()
		return redirect('products:products')
	else:
		form = NewProductForm()
	return render(request, 'products/new_product.html', { 'form':form })


class product_detail_view(DetailView):
	model = Product
	template_name = 'products/product_detail.html'


def cart(request):
	return render(request, "checkout/cart.html", {});

def checkout(request):
	return render(request, "checkout/checkout.html",  {});