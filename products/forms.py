from django import forms

from  . models import Product


class NewProductForm(forms.ModelForm):


	class Meta:
		model = Product
		fields = (
			'name',
			'price',
			'description',
            'category',
			'image',
			'seller_info',
			'seller_contact',
		)
