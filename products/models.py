# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils import timezone

from django.urls import reverse

from django.conf import settings

from django.db import models


class Product(models.Model):
    CATEGORY_CHOICES = (
    ('shirts', 'Shirt'),
    ('trousers', 'Trousers & Shorts'),
    ('footwears', 'Footwears'),
    ('others','Others'),
    ('gifts', 'Gifts'),
    )
    user 		= models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name 		= models.CharField(max_length=150)
    price 		= models.DecimalField(max_digits=9, decimal_places=2)
    description = models.CharField(max_length=500)
    slug    	= models.SlugField(max_length=150, unique_for_date='created')
    image 		= models.ImageField(upload_to='products/%Y/%m/%d')
    seller_info = models.TextField(blank=True, null=True)
    seller_contact = models.IntegerField(blank=True, null=True)
    created		= models.DateTimeField(default=timezone.now)
    updated 	= models.DateTimeField(auto_now=True)
    category    = models.CharField(max_length=25, choices=CATEGORY_CHOICES, default='others')
    is_paid     = models.BooleanField(default=False)


    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return self.name

    def price_sign(self):
        strp = str(self.price)
        return strp + " GHS"

    def get_absolute_url(self):
        return reverse('products:post_detail', args = [str(self.pk)] )
